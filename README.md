# Ubiquiti Unifi Debian repacks

This repository repacks the 'unifi' (Ubiquiti UniFi server) package by changing the MongoDB requirements.
 
The repacked deb files are available as pipeline artifacts.

## Ubiquiti UniFi server

Ubiquiti UniFi server is a centralized management system for UniFi suite of devices.

After the UniFi server is installed, the UniFi controller can be accessed on any
web browser. The UniFi controller allows the operator to instantly provision thousands
of UniFi devices, map out network topology, quickly manage system traffic, and further
provision individual UniFi devices.

https://www.ui.com/

# Notes

## Building docker Unifi downloader

~~~bash
docker build --network host -t registry.gitlab.com/jlecomte/unifi-repack .
~~~

# License

This project is licensed under the CC0 1.0 Universal - see the LICENSE file for details.
